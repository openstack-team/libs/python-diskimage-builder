python-diskimage-builder (3.30.0-4) unstable; urgency=medium

  * Switch to pybuild.
  * Blacklist test_mount_comparator (Closes: #1090268).

 -- Thomas Goirand <zigo@debian.org>  Tue, 17 Dec 2024 08:50:24 +0100

python-diskimage-builder (3.30.0-3) unstable; urgency=medium

  * Add stop-using-the-imp-module.patch (Closes: #1058248).

 -- Thomas Goirand <zigo@debian.org>  Sun, 07 Jan 2024 12:54:58 +0100

python-diskimage-builder (3.30.0-2) unstable; urgency=medium

  * Cleans better (Closes: #1048492).

 -- Thomas Goirand <zigo@debian.org>  Tue, 22 Aug 2023 11:04:37 +0200

python-diskimage-builder (3.30.0-1) unstable; urgency=medium

  * New upstream release.
  * Add python3-jsonschema (b)-depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 02 Aug 2023 14:33:23 +0200

python-diskimage-builder (3.13.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 29 Sep 2021 10:14:04 +0200

python-diskimage-builder (3.13.0-1) experimental; urgency=medium
  
  [ Kevin Allioli ]
  * New upstream release.
  * Added stestr repository and remove testrepository.

 -- Thomas Goirand <zigo@debian.org>  Tue, 24 Aug 2021 09:05:57 +0200

python-diskimage-builder (3.2.1-4) unstable; urgency=medium

  * Re-add python3-mock as build-depends. (Closes: #973189)

 -- Thomas Goirand <zigo@debian.org>  Mon, 02 Nov 2020 08:52:43 +0100

python-diskimage-builder (3.2.1-3) unstable; urgency=medium

  * Fixed (build-)depends.

 -- Thomas Goirand <zigo@debian.org>  Wed, 14 Oct 2020 11:46:50 +0200

python-diskimage-builder (3.2.1-2) unstable; urgency=medium

  * Uploading to unstable.
  * Add a debian/salsa-ci.yml and fixed watch file.

 -- Thomas Goirand <zigo@debian.org>  Wed, 14 Oct 2020 10:57:53 +0200

python-diskimage-builder (3.2.1-1) experimental; urgency=medium

  * New upstream release.
  * Removed python3-six from (build-)-depends.
  * Add python3-sphinxcontrib.apidoc to build-depends.
  * Fixed tweaking shebang.

 -- Thomas Goirand <zigo@debian.org>  Mon, 07 Sep 2020 14:19:33 +0200

python-diskimage-builder (2.36.0-1) unstable; urgency=medium

  * New upstream release.
  * Removed python3-babel from (build-)depends.

 -- Thomas Goirand <zigo@debian.org>  Fri, 08 May 2020 12:37:15 +0200

python-diskimage-builder (2.27.1-3) unstable; urgency=medium

  * Add runtime depends on fdisk (Closes: #872128).

 -- Thomas Goirand <zigo@debian.org>  Wed, 08 Jan 2020 08:37:14 +0100

python-diskimage-builder (2.27.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Oct 2019 09:10:57 +0200

python-diskimage-builder (2.27.1-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Thomas Goirand ]
  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 17 Sep 2019 22:02:58 +0200

python-diskimage-builder (2.20.3-2) unstable; urgency=medium

  * Uploading to unstable.
  * Removed versions of dependencies statisfied in Buster.

 -- Thomas Goirand <zigo@debian.org>  Tue, 16 Jul 2019 21:02:25 +0200

python-diskimage-builder (2.20.3-1) experimental; urgency=medium

  * New upstream release.
  * Removed Python 2 support.
  * Run unit tests using installed Python module.

 -- Thomas Goirand <zigo@debian.org>  Wed, 27 Mar 2019 10:35:06 +0100

python-diskimage-builder (2.16.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Use team+openstack@tracker.debian.org as maintainer

  [ Thomas Goirand ]
  * New upstream release:
    - Fixes FTBFS due to failed tests (Closes: #906551).
    - Reproducible patch applied upstream (Closes: #892020).
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 07 Sep 2018 09:49:08 +0200

python-diskimage-builder (2.10.1-6) unstable; urgency=medium

  * Add missing git command runtime depends.

 -- Thomas Goirand <zigo@debian.org>  Thu, 29 Mar 2018 14:19:19 +0200

python-diskimage-builder (2.10.1-5) unstable; urgency=medium

  * Added missing curl command runtime depends.

 -- Thomas Goirand <zigo@debian.org>  Thu, 29 Mar 2018 14:08:15 +0200

python-diskimage-builder (2.10.1-4) unstable; urgency=medium

  * Add patch to package all files.
  * Fix python / python3 shebangs.

 -- Thomas Goirand <zigo@debian.org>  Thu, 29 Mar 2018 10:42:10 +0200

python-diskimage-builder (2.10.1-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * Running wrap-and-sort -bast

  [ Thomas Goirand ]
  * Fixed Python 3 shebang.

 -- Thomas Goirand <zigo@debian.org>  Sun, 18 Mar 2018 21:14:52 +0000

python-diskimage-builder (2.10.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 25 Feb 2018 22:56:29 +0000

python-diskimage-builder (2.10.1-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Updated debian/copyright.
  * Standards-Version is now 4.1.3.
  * Add Python 3 support.
  * Add a -doc package.

 -- Thomas Goirand <zigo@debian.org>  Mon, 19 Feb 2018 14:17:28 +0000

python-diskimage-builder (2.9.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bumped debhelper compat version to 10

  [ Thomas Goirand ]
  * Updating vcs fields.
  * Updating copyright format url.
  * Running wrap-and-sort -bast.
  * Updating maintainer field.
  * Standards-Version: 4.1.1
  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 07 Nov 2017 21:04:19 +0100

python-diskimage-builder (1.19.1-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URLs.
  * d/rules: Changed UPSTREAM_GIT protocol to https
  * d/s/options: extend-diff-ignore of .gitreview
  * d/control: Using OpenStack's Gerrit as VCS URLs.

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Standards-Version: 3.9.8 (no change).
  * Add debian/python-diskimage-builder.lintian-overrides

 -- Thomas Goirand <zigo@debian.org>  Tue, 27 Sep 2016 22:54:19 +0200

python-diskimage-builder (1.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Depends on jq (Closes: #783517).
  * Now depends on uuid-runtime (Closes: #791655).

 -- Thomas Goirand <zigo@debian.org>  Sat, 08 Aug 2015 09:56:02 +0000

python-diskimage-builder (0.1.46-1) unstable; urgency=medium

  * New upstream release (Closes: #788096).
  * Fixed (build-)dependencies.
  * Cleans leftovers .pyc in the package after python setup.py install.

 -- Thomas Goirand <zigo@debian.org>  Tue, 09 Jun 2015 10:21:41 +0200

python-diskimage-builder (0.1.30-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 16 Sep 2014 14:33:25 +0800

python-diskimage-builder (0.1.13-2) unstable; urgency=medium

  [ Thomas Bechtold ]
  * Add debian/clean file to cleanup egg-info and .testrepository directory.
  * debian/gbp.conf: Don't use pristine-tar explicit.
  * debian/control:
    - Add python3 packages to Build-Depends.
    - Add kpartx to Depends according to README.md.
    - Add debootstrap to Depends. Needed to build Debian images.
  * debian/watch: Use github tags instead of pypi.

  [ Thomas Goirand ]
  * New upstream release.
  * Uploading to unstable.
  * Adds qemu-utils (and *NOT* qemu-img) as depends.

 -- Thomas Goirand <zigo@debian.org>  Fri, 18 Apr 2014 10:46:29 +0000

python-diskimage-builder (0.1.8-2) experimental; urgency=medium

  * Added missing subunit build-depends.

 -- Thomas Goirand <zigo@debian.org>  Sun, 16 Mar 2014 09:17:57 +0000

python-diskimage-builder (0.1.8-1) experimental; urgency=low

  * Initial release. (Closes: #723823)

 -- Thomas Goirand <zigo@debian.org>  Sat, 15 Mar 2014 17:13:48 +0800
